import { Routes, RouterModule } from '@angular/router';
import { PopularMoviesComponent } from './movie/pages/popular-movies/popular-movies.component';
import { MovieDetailsComponent } from './movie/pages/movie-details/movie-details.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CastListComponent } from './movie/pages/cast-list/cast-list.component';
import { LoginComponent } from './core/pages/login/login.component';
import { ReviewListComponent } from './movie/pages/review-list/review-list.component';
import { SignUpComponent } from './core/pages/sign-up/sign-up.component';
import { LoggedUserGuard } from './core/guards/logged-user/logged-user.guard';
import { FavoriteMoviesComponent } from './movie/pages/favorite-movies/favorite-movies.component';
import { AuthenticationGuard } from './core/guards/auth-guard/authentication.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'movies'
  },
  {
    path: 'login',
    pathMatch: 'full',
    canActivate: [LoggedUserGuard],
    component: LoginComponent
  },
  {
    path: 'sign-up',
    pathMatch: 'full',
    canActivate: [LoggedUserGuard],
    component: SignUpComponent
  },
  {
    path: 'movies',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'popular'
      },
      {
        path: 'popular',
        pathMatch: 'full',
        component: PopularMoviesComponent
      },
      {
        path: 'favorites',
        pathMatch: 'full',
        canActivate: [AuthenticationGuard],
        component: FavoriteMoviesComponent,
      },
      {
        path: ':id/cast',
        pathMatch: 'full',
        component: CastListComponent
      },
      {
        path: ':id/reviews',
        pathMatch: 'full',
        component: ReviewListComponent
      },
      {
        path: ':id',
        pathMatch: 'full',
        component: MovieDetailsComponent
      },
    ],
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  },
];

export const AppRoutingModule = RouterModule.forRoot(routes, {
  scrollPositionRestoration: 'top',
});
