import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutesToHours',
})
export class MinutesToHoursPipe implements PipeTransform {
  transform(num: number): string {
    let result: string;

    if (num % 60 === 0) {
      result = `${num}h`;
    } else if (num > 60) {
      result = `${Math.ceil(num / 60)}h ${num % 60}m`;
    } else if (num < 60) {
      result = `${num}m`;
    }

    return result;
  }
}
