import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Input() currentPage: number;
  @Input() maxPages: number;
  @Input() pageToRoute: string;

  constructor() {}

  ngOnInit(): void {}

  get paginations(): number[] {
    const pages = [];
    if (this.currentPage < 4) {
      for (let i = 0; i <= 5; i++) {
        pages.push(i);
      }
    } else if (this.currentPage >= 4) {
      for (let i = 1; i <= 3; i++) {
        pages.unshift(+this.currentPage - i + 1);
        pages.push(+this.currentPage + i);
      }
      pages.pop();
    }

    return pages.filter((page) => page > 0 && page <= this.maxPages);
  }

  get showFirst(): boolean {
    if (this.currentPage > 3) {
      return true;
    }
  }

  get showLast(): boolean {
    if (this.currentPage < this.maxPages - 2) {
      return true;
    }
  }
}
