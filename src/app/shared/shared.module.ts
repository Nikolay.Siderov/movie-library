import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination/pagination.component';
import { RouterModule } from '@angular/router';
import { ButtonBackComponent } from './button-back/button-back.component';
import { MinutesToHoursPipe } from './pipes/minutes-to-hours/minutes-to-hours.pipe';


@NgModule({
  declarations: [PaginationComponent, ButtonBackComponent, MinutesToHoursPipe],
  imports: [CommonModule, RouterModule],
  exports: [PaginationComponent, ButtonBackComponent, MinutesToHoursPipe],
})
export class SharedModule {}
