import { Component, OnInit, Input } from '@angular/core';
import { baseUrl, defaultActorImgSizeList } from 'src/app/movie/components/movie-img/constants';
import { ICast } from '../../entities/cast.interface';
import { defaultActorImageUrl } from './constants';

@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.scss']
})
export class ActorComponent implements OnInit {
  imgUrl: string;
  @Input() actor: ICast;

  constructor( ) { }

  ngOnInit(): void {
    if (this.actor.profile_path){
      this.imgUrl = `${baseUrl}${defaultActorImgSizeList}${this.actor.profile_path}`;
    }else{
      this.imgUrl = defaultActorImageUrl;
    }
  }
}
