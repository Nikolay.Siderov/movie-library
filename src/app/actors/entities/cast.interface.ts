import { ICrew } from './crew.interface';

export interface ICast {
  cast_id: number;
  character: string;
  credit_id: string;
  gender: number;
  id: number;
  name: string;
  order: number;
  profile_path: string;
}

export interface ICastHttpResponse{
  id: number;
  cast: ICast[];
  crew: ICrew[];
}
