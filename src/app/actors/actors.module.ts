import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActorComponent } from './components/actor/actor.component';


@NgModule({
  declarations: [ActorComponent],
  imports: [
    CommonModule
  ],
  exports: [ActorComponent]
})
export class ActorsModule { }
