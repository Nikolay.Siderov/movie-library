import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IMovieDetails } from '../../entities/movie.interface';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/entities/app-state';
import { addToFavorites, removeFromFavorites } from 'src/app/store/actions/favorite-movies.actions';
import { IFavoriteMoviesList } from 'src/app/store/entities/fav-movies-list.interface';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-to-favorite',
  templateUrl: './add-to-favorite.component.html',
  styleUrls: ['./add-to-favorite.component.scss']
})
export class AddToFavoriteComponent implements OnInit, OnDestroy {

  @Input() movie: IMovieDetails;
  isFavorite: boolean;
  isUserLoged: boolean;
  store$: Subscription;

  constructor(
    private store: Store<IAppState>,
    private auth: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.auth.isUserLogged.subscribe((isLogged: string | boolean | null) => {

      this.isUserLoged = !!isLogged;

      if (isLogged){
        this.store$ = this.store.select(state => state.global.favoriteMovies)
        .subscribe((favoriteMovies: IFavoriteMoviesList) => {
          if (favoriteMovies){
            this.isFavorite = !!favoriteMovies.hasOwnProperty(this.movie.id);
          }
        });
      }
    });

  }

  addToFavorites(): void{
    if (this.isUserLoged){
      this.store.dispatch(addToFavorites(this.movie));
    }else{
      this.router.navigateByUrl('/login');
    }
  }

  removeFromFavorites(): void{
    this.store.dispatch(removeFromFavorites({movieId: this.movie.id}));
  }

  ngOnDestroy(): void{
    if (this.isUserLoged){
      this.store$.unsubscribe();
    }
  }
}
