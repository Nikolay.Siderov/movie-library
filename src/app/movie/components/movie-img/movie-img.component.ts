import { Component, OnInit, Input } from '@angular/core';
import { baseUrl, vailablePosterSizes, defaultPosterSize } from './constants';

@Component({
  selector: 'app-movie-img',
  templateUrl: './movie-img.component.html',
  styleUrls: ['./movie-img.component.scss']
})
export class MovieImgComponent implements OnInit {
  @Input() posterPath?: string;
  @Input() posterSize: string;
  url: string;

  constructor() { }

  ngOnInit(): void {
    if (!vailablePosterSizes.includes(this.posterSize)){
      this.posterSize = defaultPosterSize;
    }
    if (this.posterPath){
      this.url = `${baseUrl}${this.posterSize}${this.posterPath}`;
    }
  }
}
