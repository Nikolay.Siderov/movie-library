import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieImgComponent } from './movie-img.component';

describe('MovieImgComponent', () => {
  let component: MovieImgComponent;
  let fixture: ComponentFixture<MovieImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieImgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
