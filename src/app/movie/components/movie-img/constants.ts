export const baseUrl = 'https://image.tmdb.org/t/p/';
export const defaultPosterSize = 'w500';
export const defaultActorImgSizeList = 'w92';

export const vailablePosterSizes = [
    'w92',
    'w154',
    'w185',
    'w342',
    'w500',
    'w780',
    'original',
  ];