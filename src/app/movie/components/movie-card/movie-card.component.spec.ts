import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieCardComponent } from './movie-card.component';
import { CommonModule, DatePipe } from '@angular/common';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieCardComponent, DatePipe ],
      imports: [ CommonModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    component.movie = {
      poster_path: 'www.google.com',
      adult: true,
      overview: 'asdsadsad',
      genres: [{
        id: '10',
        name: 'Sci-fi'
      }],
      release_date: '2020-05-10',
      genre_ids: [15, 20, 30],
      original_title: 'Spiderman',
      id: 324232,
      original_language: 'BG',
      title: 'Spiderman',
      backdrop_path: 'sadsada',
      popularity: 7.5,
      vote_count: 6,
      video: false,
      vote_average: 7.5
    };
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
