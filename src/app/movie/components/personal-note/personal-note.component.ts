import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/entities/app-state';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { updatePersonalNote } from 'src/app/store/actions/favorite-movies.actions';
import { Subscription } from 'rxjs';
import { IFavoriteMoviesList } from 'src/app/store/entities/fav-movies-list.interface';

@Component({
  selector: 'app-personal-note',
  templateUrl: './personal-note.component.html',
  styleUrls: ['./personal-note.component.scss'],
})
export class PersonalNoteComponent implements OnInit, OnDestroy {
  @Input() movieId: number;

  noteForm: FormGroup;
  personalNote: string;
  isUserLogged: boolean;
  isFormToggled: boolean;
  isMovieFavorite: boolean;
  store$: Subscription;
  auth$: Subscription;

  constructor(
    private store: Store<IAppState>,
    private auth: AuthenticationService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.noteForm = this.fb.group({
      noteContent: ['', Validators.required],
    });

    // Used to show and hide adding note section depending on user status
    this.auth$ = this.auth.isUserLogged.subscribe((isUserLoged: string | boolean | null) => {
      this.isUserLogged = !!isUserLoged;
    });

    // Subscribes and checks the redux store if the movie is favorite allows to add personal note
    this.store$ = this.store.select((data) => data.global.favoriteMovies)
      .subscribe((favoriteMovies: IFavoriteMoviesList | null) => {

        if (favoriteMovies) {

          if ( favoriteMovies.hasOwnProperty(this.movieId) &&
            favoriteMovies[this.movieId].personalNote !== null ) {

            this.isMovieFavorite = true;
            this.personalNote = favoriteMovies[this.movieId].personalNote;

          } else {

            this.isMovieFavorite = false;
          }
        }
      });
  }

  // If the note input field is valid and the movie is favorite dispatches action for updating note
  submitNote(): void {
    if (!this.noteForm.invalid && this.isMovieFavorite) {

      const newNote: string = this.noteForm.value.noteContent;

      this.store.dispatch(
        updatePersonalNote({
          personalNote: newNote,
          movieId: this.movieId,
        })
      );

      this.noteForm.reset();
      this.isFormToggled = false;
    }
  }

  // Allows to toggle form only if the movies is added to favorites list
  showForm(): void {
    if (this.isMovieFavorite){
      this.isFormToggled = !this.isFormToggled;
    }
  }

  ngOnDestroy(): void {
    this.store$.unsubscribe();
    this.auth$.unsubscribe();
  }
}
