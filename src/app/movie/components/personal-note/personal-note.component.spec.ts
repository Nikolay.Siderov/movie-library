import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalNoteComponent } from './personal-note.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from 'src/app/store/reducers/favorite-movies.reducer';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('PersonalNoteComponent', () => {
  let component: PersonalNoteComponent;
  let fixture: ComponentFixture<PersonalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalNoteComponent ],
      imports: [StoreModule.forRoot(reducers), HttpClientModule, RouterTestingModule, ReactiveFormsModule],
      providers: [AuthenticationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
