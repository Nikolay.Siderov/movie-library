import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReviewComponent } from './user-review.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UserReviewComponent', () => {
  let component: UserReviewComponent;
  let fixture: ComponentFixture<UserReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReviewComponent ],
      imports: [BrowserAnimationsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReviewComponent);
    component = fixture.componentInstance;
    component.review = {
      author: 'Ivan',
      content: 'Lorem Ipsum',
      id: 'as734iad',
      url: 'www.google.com'
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
