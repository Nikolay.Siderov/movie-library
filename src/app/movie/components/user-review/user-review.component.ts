import { Component, OnInit, Input } from '@angular/core';
import { IReview } from '../../entities/review.interface';
import { expandReview } from './user-review.animations';

@Component({
  selector: 'app-user-review',
  templateUrl: './user-review.component.html',
  styleUrls: ['./user-review.component.scss'],
  animations: expandReview,
})
export class UserReviewComponent implements OnInit {
  @Input() review: IReview;

  contentState = 'shrink';
  showMoreContent = false;
  buttonContent = 'Read';
  defaultVisibleLenght = 400;
  disableReadMoreBtn: boolean;

  constructor() {}

  ngOnInit(): void {
    this.disableReadMoreBtn =
      this.review.content.length > this.defaultVisibleLenght ? false : true;
  }

  showMore() {
    this.contentState = !this.showMoreContent ? 'grow' : 'shrink';

    this.buttonContent = !this.showMoreContent ? 'Hide' : 'Read';

    setTimeout(() => {
      this.showMoreContent = !this.showMoreContent;
    }, 1000);
  }
}
