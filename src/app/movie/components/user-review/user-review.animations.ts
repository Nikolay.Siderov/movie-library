import { trigger, state, style, transition, animate } from '@angular/animations';

export const expandReview = [trigger('expandState', [
    state('shrink', style({
      'height': '80px',
      'overflow': 'hidden'
    })),
    state('grow', style({
      'max-height': '4000px',
      'overflow': 'hidden'
    })),
    transition('shrink <=> grow', animate(1000))
  ])
]