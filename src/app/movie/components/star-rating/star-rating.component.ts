import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss'],
})
export class StarRatingComponent implements OnInit {
  stars: string;
  @Input() readonly rating: number;
  @Input() size ? = 'w100';

  constructor() {}

  ngOnInit(): void {
    let rating = this.rating / 2;
    rating = Math.ceil(rating / 0.5) * 0.5;
    this.stars = `../../../assets/stars-rating/Star_rating_${rating}_of_5.png`;
  }

  get percent() {
    return this.rating / 10;
  }
}
