import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { API_URL, HTTP_PATHS, QUERY, API_KEY } from 'src/app/core/constants';
import { Observable } from 'rxjs';
import { IActionMovieID } from 'src/app/store/entities/store.interface';
import { IMovieDetails } from '../entities/movie.interface';

@Injectable()
export class FavoriteMoviesService {
  constructor(private http: HttpClient, private auth: AuthenticationService) {}

  getFavorites(): Observable<any> {
    return this.http.get(
      `${API_URL.favorites}${HTTP_PATHS.favorites}/${this.auth.localId}${HTTP_PATHS.json}${QUERY.key}${API_KEY.favorites}`
    );
  }

  addToFavorites(movie: IMovieDetails): Observable<any> {
    return this.http.put(
      `${API_URL.favorites}${HTTP_PATHS.favorites}/${this.auth.localId}/${movie.id}${HTTP_PATHS.json}${QUERY.key}${API_KEY.favorites}`,
      JSON.stringify(movie)
    );
  }

  removeFromFavorites(action: IActionMovieID): Observable<any>{
    return this.http.delete(
      `${API_URL.favorites}${HTTP_PATHS.favorites}/${this.auth.localId}/${action.movieId}${HTTP_PATHS.json}${QUERY.key}${API_KEY.favorites}`
    );
  }

  updateNote(movieId: number, newNote: string): Observable<any>{
    return this.http.patch(
      `${API_URL.favorites}${HTTP_PATHS.favorites}/${this.auth.localId}/${movieId}${HTTP_PATHS.json}${QUERY.key}${API_KEY.favorites}`,
      {
        personalNote: newNote
      }
    );
  }
}
