import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_KEY, API_URL, QUERY } from '../../core/constants';
import { Observable } from 'rxjs';
import { ICastHttpResponse } from '../../actors/entities/cast.interface';
import { IReviewHttpResponse } from '../entities/review.interface';
import { IMovieHttpResponse, IMovieDetails } from '../entities/movie.interface';

@Injectable()
export class MovieService {
  private readonly movie: string = '/movie';
  private readonly popular: string = '/popular';
  private readonly cast: string = '/credits';
  private readonly reviews: string = '/reviews';
  private readonly recommendations: string = '/recommendations';
  private readonly upcoming: string = '/upcoming';

  constructor(private http: HttpClient) {}

  getUpcomming(): Observable<IMovieHttpResponse> {
    return this.http.get<IMovieHttpResponse>(
      `${API_URL.TMDB}${this.movie}${this.upcoming}${QUERY.key}${API_KEY.TMDB}`
    );
  }

  getCast(movieId: number): Observable<ICastHttpResponse> {
    return this.http.get<ICastHttpResponse>(
      `${API_URL.TMDB}${this.movie}/${movieId}${this.cast}${QUERY.apiKey}${API_KEY.TMDB}`
    );
  }

  getReviews(movieId: number): Observable<IReviewHttpResponse> {
    return this.http.get<IReviewHttpResponse>(
      `${API_URL.TMDB}${this.movie}/${movieId}${this.reviews}${QUERY.apiKey}${API_KEY.TMDB}`
    );
  }

  getRecommended(movieId: number): Observable<IMovieHttpResponse> {
    return this.http.get<IMovieHttpResponse>(
      `${API_URL.TMDB}${this.movie}/${movieId}${this.recommendations}${QUERY.apiKey}${API_KEY.TMDB}`
    );
  }

  getMovieById(id: number): Observable<IMovieDetails> {
    return this.http.get<IMovieDetails>(
      `${API_URL.TMDB}${this.movie}/${id}${QUERY.apiKey}${API_KEY.TMDB}`
    );
  }

  getPopularMovies(page = 1): Observable<IMovieHttpResponse> {
    return this.http.get<IMovieHttpResponse>(
      `${API_URL.TMDB}${this.movie}${this.popular}${QUERY.apiKey}${API_KEY.TMDB}${QUERY.page}${page}`
    );
  }

  searchMovie(searchQuery: string): Observable<IMovieHttpResponse> | boolean[] {
    if (searchQuery) {
      return this.http.get<IMovieHttpResponse>(
        `${API_URL.TMDB}/search${this.movie}${QUERY.apiKey}${API_KEY.TMDB}${QUERY.query}${searchQuery}`
      );
    } else {
      return [false];
    }
  }
}
