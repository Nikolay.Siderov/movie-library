import { Component } from '@angular/core';
import { IAppState } from 'src/app/store/entities/app-state';
import { Store } from '@ngrx/store';
import { removeFromFavorites } from 'src/app/store/actions/favorite-movies.actions';

@Component({
  selector: 'app-favorite-movies',
  templateUrl: './favorite-movies.component.html',
  styleUrls: ['./favorite-movies.component.scss']
})
export class FavoriteMoviesComponent {

  movies$ = this.store.select(state => state.global.favoriteMovies);

  constructor(
    private store: Store<IAppState>
  ) { }

  removeMovie(movieId: number): void{
    this.store.dispatch(removeFromFavorites({movieId}));
  }
}
