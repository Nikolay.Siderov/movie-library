import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from 'src/app/movie/services/movies.service';
import { IMovie, IMovieHttpResponse, IMovieDetails } from '../../entities/movie.interface';
import { ICast, ICastHttpResponse } from 'src/app/actors/entities/cast.interface';
import { IReview, IReviewHttpResponse } from '../../entities/review.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
})
export class MovieDetailsComponent implements OnInit, OnDestroy {
  movie: IMovieDetails;
  private obsArray = [];
  private movie$: Subscription;
  private cast$: Subscription;
  private review$: Subscription;
  private recommended$: Subscription;

  private _recommendedList: IMovie[] = [];
  private _castList: ICast[] = [];
  private _reviewListArr: IReview[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private movieService: MovieService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const movieId: number = +this.activatedRoute.snapshot.params.id;

    this.movie$ = this.movieService.getMovieById(movieId).subscribe(
      (response: IMovieDetails) => (this.movie = response)
    );

    this.cast$ = this.movieService.getCast(movieId)
      .subscribe((cast: ICastHttpResponse) => {
        this._castList = cast.cast;
      });

    this.review$ = this.movieService
      .getReviews(movieId)
      .subscribe((review: IReviewHttpResponse) => {
        this._reviewListArr = review.results;
      });

    this.recommended$ = this.movieService
      .getRecommended(movieId)
      .subscribe((data: IMovieHttpResponse) => {
        this._recommendedList = data.results;
      });

    this.obsArray.push(
      this.movie$,
      this.cast$,
      this.review$,
      this.recommended$
    );
  }

  get movieGenres(): string {
    return this.movie.genres.map((genre) => genre.name).join(', ');
  }

  // Cast section

  get castList(): ICast[] {
    return this._castList.slice(0, 10);
  }

  // Review section

  get isReviewListExceed(): boolean{
    return this._reviewListArr.length > 3;
  }

  get reviewList(): IReview[] | boolean {
    if (this._reviewListArr.length === 0) {
      return false;
    }else{
      return this._reviewListArr.slice(0, 3);
    }
  }

  // Recomended movies

  get recommendedList(): IMovie[] {
    if (this._recommendedList.length) {
      return this._recommendedList;
    }
  }

  ngOnDestroy(): void {
    this.obsArray.forEach((e) => e.unsubscribe());
  }
}
