import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from '../../services/movies.service';
import { ICast, ICastHttpResponse } from 'src/app/actors/entities/cast.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cast-list',
  templateUrl: './cast-list.component.html',
  styleUrls: ['./cast-list.component.scss'],
})
export class CastListComponent implements OnInit, OnDestroy {
  movieId: number;
  castList: ICast[];
  cast$: Subscription;

  constructor(
    private router: ActivatedRoute,
    private movieService: MovieService,
  ) {}

  ngOnInit(): void {
    this.movieId = +this.router.snapshot.paramMap.get('id');

    this.cast$ = this.movieService
      .getCast(this.movieId)
      .subscribe((cast: ICastHttpResponse) => {
        this.castList = cast.cast;
      });
  }

  ngOnDestroy(): void {
    this.cast$.unsubscribe()
  }
}
