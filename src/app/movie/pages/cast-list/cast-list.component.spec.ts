import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CastListComponent } from './cast-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieService } from '../../services/movies.service';
import { HttpClientModule } from '@angular/common/http';

describe('CastListComponent', () => {
  let component: CastListComponent;
  let fixture: ComponentFixture<CastListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CastListComponent ],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [MovieService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CastListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
