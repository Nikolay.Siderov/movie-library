import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MovieService } from 'src/app/movie/services/movies.service';
import { IMovie, IMovieHttpResponse } from '../../entities/movie.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';
import { map, switchMap, debounceTime, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-popular-movies',
  templateUrl: './popular-movies.component.html',
  styleUrls: ['./popular-movies.component.scss'],
})
export class PopularMoviesComponent implements OnInit, OnDestroy, AfterViewInit {

  currentPage: number;
  maxPages: number;
  movies: IMovie[];
  isSearch = false;
  searchMoviesResult: IMovie[];
  searchEvent$: Subscription;
  movies$: Subscription;

  @ViewChild('searchMovieInput', { static: false }) searchMovieInput: ElementRef;

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((param) => {
      if (!param.hasOwnProperty('page')) {
        this.router.navigate(['/movies/popular'], { queryParams: { page: 1 } });
      }

      this.currentPage = param.page || 1;

      this.movies$ = this.movieService
        .getPopularMovies(this.currentPage)
        .subscribe(
          (moviesData: IMovieHttpResponse) => {
            this.movies = moviesData.results;
            this.maxPages = moviesData.total_pages;
          },
          (error: HttpErrorResponse) => {
            this.router.navigate(['/movies/popular'], {
              queryParams: { page: 1 },
            });
          }
        );
    });
  }

  ngAfterViewInit() {
    this.searchEvent$ = fromEvent(this.searchMovieInput.nativeElement, 'keyup')
      .pipe(
        debounceTime(500),
        map( (searchData: KeyboardEvent) => (searchData.target as HTMLInputElement).value ),
        switchMap((data: string) => this.movieService.searchMovie(data)),
        tap((data: IMovieHttpResponse) => {
          if (data) {
            this.isSearch = true;
            this.searchMoviesResult = data.results;
          } else {
            this.isSearch = false;
          }
        }))
      .subscribe();
  }

  ngOnDestroy() {
    this.searchEvent$.unsubscribe();
    this.movies$.unsubscribe();
  }
}
