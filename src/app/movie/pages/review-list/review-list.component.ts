import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from '../../services/movies.service';
import { IReview } from '../../entities/review.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.scss']
})
export class ReviewListComponent implements OnInit, OnDestroy {
  movieId: number;
  reviewList$: Subscription;
  reviewList: IReview[]

  constructor(
    private router: ActivatedRoute,
    private movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.movieId = +this.router.snapshot.paramMap.get('id')
    this.reviewList$ = this.movieService
      .getReviews(this.movieId)
      .subscribe((response: any) =>{
        this.reviewList = response.results;
    })
  }

  ngOnDestroy(): void{
    this.reviewList$.unsubscribe();
  }

}
