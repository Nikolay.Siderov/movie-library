import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { PopularMoviesComponent } from './pages/popular-movies/popular-movies.component';
import { MovieDetailsComponent } from './pages/movie-details/movie-details.component';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import { ActorsModule } from '../actors/actors.module';
import { SharedModule } from '../shared/shared.module';
import { UserReviewComponent } from './components/user-review/user-review.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MovieService } from './services/movies.service';
import { HttpClientModule } from '@angular/common/http';
import { CastListComponent } from './pages/cast-list/cast-list.component';
import { MovieImgComponent } from './components/movie-img/movie-img.component';
import { ReviewListComponent } from './pages/review-list/review-list.component';
import { AddToFavoriteComponent } from './components/add-to-favorite-btn/add-to-favorite.component';
import { FavoriteMoviesComponent } from './pages/favorite-movies/favorite-movies.component';
import { FavoriteMoviesService } from './services/favorite-movies.service';
import { EffectsModule } from '@ngrx/effects';
import { FavoriteMoviesEffects } from '../store/effects/favorite-movies.effects';
import { PersonalNoteComponent } from './components/personal-note/personal-note.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    MovieCardComponent,
    PopularMoviesComponent,
    MovieDetailsComponent,
    StarRatingComponent,
    UserReviewComponent,
    CastListComponent,
    MovieImgComponent,
    ReviewListComponent,
    AddToFavoriteComponent,
    FavoriteMoviesComponent,
    PersonalNoteComponent
  ],
  imports: [
    CommonModule,
    ActorsModule,
    SharedModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    EffectsModule.forFeature([FavoriteMoviesEffects]),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    MovieService,
    FavoriteMoviesService
  ]
})
export class MovieModule { }


