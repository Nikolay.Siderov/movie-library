export interface IMovie {
  poster_path: string;
  adult: boolean;
  overview: string;
  genres: [
    {
      id: string;
      name: string;
    }
  ];
  release_date: string;
  genre_ids: Array<number>;
  id: number;
  original_title: string;
  original_language: string;
  title: string;
  backdrop_path: string;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
}

export interface IMovieHttpResponse {
  page: number;
  results: IMovie[];
  total_pages: number;
  total_results: number;
}

export interface IMovieDetails extends IMovie {
  belongs_to_collection: any;
  budget: number;
  homepage: string;
  imdb_id: string;
  production_companies: Array<{
    id: number;
    logo_path: any;
    name: string;
    origin_country: string;
  }>;
  production_countries: Array<any>;
  revenue: number;
  runtime: number;
  spoken_languages: Array<{ iso_639_1: string; name: string }>;
  status: string;
  tagline: string;
  personalNote?: string;
}
