export interface IReview {
  author: string;
  content: string;
  id: string;
  url: string;
}

export interface IReviewHttpResponse{
  id: number;
  page: number;
  results: IReview[];
  total_pages: number;
  total_results: number;
}
