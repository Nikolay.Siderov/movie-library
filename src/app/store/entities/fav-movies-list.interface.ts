import { IMovieDetails } from '../../movie/entities/movie.interface';
export interface IFavoriteMoviesList {
    [key: number]: IMovieDetails;
    type?: string;
}
