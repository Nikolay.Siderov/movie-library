import { IFavoriteMoviesList } from './fav-movies-list.interface';

export interface IFavoriteMoviesStore{
    favoriteMovies: IFavoriteMoviesList;
}

export interface IActionMovieID{
    movieId: number;
    type: string;
}
