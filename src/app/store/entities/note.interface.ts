export interface IUpdateNoteAction{
    personalNote: string;
    movieId: number;
    type?: string;
}
