import { IFavoriteMoviesStore } from './store.interface';
export interface IAppState {
    global: IFavoriteMoviesStore;
}
