import { createReducer, on, ActionReducerMap } from '@ngrx/store';
import { IFavoriteMoviesStore, IActionMovieID } from '../entities/store.interface';
import { IFavoriteMoviesList } from '../entities/fav-movies-list.interface';
import { IAppState } from '../entities/app-state';
import { loadFavoritesSuccess, addToFavoritesSuccess, removeFromFavoritesSuccess } from '../actions/favorite-movies.actions';
import { IMovieDetails } from '../../movie/entities/movie.interface';
import { userLogout } from '../actions/authentication.action';

const initialState = {
    favoriteMovies: null
};

export const favoriteMoviesReduces = createReducer<IFavoriteMoviesStore>(
    initialState,

    on(loadFavoritesSuccess, (state: IFavoriteMoviesStore, favoriteMovies: IFavoriteMoviesList) => {
        const newObj = {...favoriteMovies};
        delete newObj.type;
        return { ...state, favoriteMovies: newObj};
    }),

    on(addToFavoritesSuccess, (state: IFavoriteMoviesStore, favoriteMovie: IMovieDetails) => {
        const movieId = favoriteMovie.id;
        const newState = {
            ...state, favoriteMovies:
            {...state.favoriteMovies, [movieId]: favoriteMovie}
        };

        return newState;
    }),

    on(removeFromFavoritesSuccess, (state: IFavoriteMoviesStore, movie: IActionMovieID) => {
        const favoriteMovies = {...state.favoriteMovies};
        delete favoriteMovies[movie.movieId];
        const newState = {...state, favoriteMovies};

        return newState;
    }),

    on(userLogout, (state: IFavoriteMoviesStore) => {
        return {...state, favoriteMovies: null};
    })
);

export const reducers: ActionReducerMap<IAppState> = { global: favoriteMoviesReduces };
