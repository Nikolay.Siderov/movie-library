import { createAction } from '@ngrx/store';

export const userLogout = createAction(
    '[USER] Logout',
);
