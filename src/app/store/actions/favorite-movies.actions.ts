import { createAction, props } from '@ngrx/store';
import { IMovieDetails } from '../../movie/entities/movie.interface';
import { IFavoriteMoviesList } from '../entities/fav-movies-list.interface';
import { IUpdateNoteAction } from '../entities/note.interface';

export const addToFavorites = createAction(
    '[MOVIE FAVORITES] Add Movie',
    props<IMovieDetails> ()
);

export const addToFavoritesSuccess = createAction(
    '[MOVIE FAVORITES] Add Movie Success',
    props<IMovieDetails> ()
);

export const loadFavorites = createAction(
    '[MOVIE FAVORITES EFFECT] Load Favorites'
);

export const loadFavoritesSuccess = createAction(
    '[MOVIE FAVORITES EFFECT] Load Favorites Success',
    props<IFavoriteMoviesList> ()
);

export const removeFromFavorites = createAction(
    '[MOVIE FAVORITES] Remove movie',
    props<{movieId: number}> ()
);

export const removeFromFavoritesSuccess = createAction(
    '[MOVIE FAVORITES] Remove Movie Success',
    props<{movieId: number}> ()
);

export const updatePersonalNote = createAction(
    '[MOVIE FAVORITES] Update Note',
    props<IUpdateNoteAction> ()
);
