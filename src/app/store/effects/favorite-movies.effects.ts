import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { loadFavorites,
  loadFavoritesSuccess, addToFavorites,
  addToFavoritesSuccess, removeFromFavorites,
  removeFromFavoritesSuccess, updatePersonalNote } from '../actions/favorite-movies.actions';
import { FavoriteMoviesService } from '../../movie/services/favorite-movies.service';
import { mergeMap, map } from 'rxjs/operators';
import { IMovieDetails } from '../../movie/entities/movie.interface';
import { IActionMovieID } from '../entities/store.interface';
import { IFavoriteMoviesList } from '../entities/fav-movies-list.interface';
import { IUpdateNoteAction } from '../entities/note.interface';



@Injectable()
export class FavoriteMoviesEffects {

  loadFavorites$ = createEffect(() => this.actions$.pipe(
    ofType(loadFavorites),
    mergeMap(() => this.moviesService.getFavorites()
    .pipe(
        map((movies: IFavoriteMoviesList) => loadFavoritesSuccess(movies))
    ))
  ));

  addToFavorites$ = createEffect(() => this.actions$.pipe(
    ofType(addToFavorites.type),
    mergeMap((movieData: IMovieDetails) => this.moviesService.addToFavorites(movieData)
    .pipe(
        map((movie: IMovieDetails) => addToFavoritesSuccess(movie))
    ))
  ));

  removeFromFavorites$ = createEffect(() => this.actions$.pipe(
    ofType(removeFromFavorites),
    mergeMap((movieData: IActionMovieID) => this.moviesService.removeFromFavorites(movieData).pipe(
        map(() => removeFromFavoritesSuccess(movieData))
    ))
  ));

  updateNote$ = createEffect(() => this.actions$.pipe(
    ofType(updatePersonalNote.type),
    mergeMap((movieData: IUpdateNoteAction) => this.moviesService.updateNote(movieData.movieId, movieData.personalNote)
      .pipe(
        map(() => loadFavorites())
    ))
  ));

constructor(
    private actions$: Actions,
    private moviesService: FavoriteMoviesService
) {}

}
