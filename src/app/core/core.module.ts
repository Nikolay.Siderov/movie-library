import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/nav/nav.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { AuthenticationGuard } from './guards/auth-guard/authentication.guard';
import { SignUpComponent } from './pages/sign-up/sign-up.component';

@NgModule({
  declarations: [NavComponent, LoginComponent, SignUpComponent],
  imports: [CommonModule, RouterModule, HttpClientModule, ReactiveFormsModule],
  exports: [NavComponent],
  providers: [AuthenticationService, AuthenticationGuard],
})
export class CoreModule {}
