export interface ISignUpForm{
    email: string;
    password: string
}

export interface ISignUpHttpResponse{
    email: string;
    expiresIn: string;
    idToken: string;
    kind: string;
    localId: string;
    refreshToken: string;
}