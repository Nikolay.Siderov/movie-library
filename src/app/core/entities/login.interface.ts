export interface ILoginForm {
  email: string;
  password: string;
}

export interface ILoginRequestResponse {
  displayName: string;
  email: string;
  idToken: string;
  kind: string;
  localId: string;
  registered: boolean;
}
