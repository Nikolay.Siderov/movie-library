import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/entities/app-state';
import { loadFavorites } from 'src/app/store/actions/favorite-movies.actions';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  loggedUser: boolean;
  favoritesMoviesQuanity: number;
  constructor(
    private authService: AuthenticationService,
    private store: Store<IAppState>
    ) {}

  /*
  * Checks if the user is logged:
  * 1. Updates nav-bar
  * 2. Updates the counter for favorite movies at 'Favorites' button
  */

  ngOnInit(): void {
    this.authService.isUserLogged.subscribe((data) => {
      this.loggedUser = !!data;

      if (this.loggedUser){
        this.store.dispatch(loadFavorites());
        this.store.select(store => store.global.favoriteMovies).subscribe(movies => {
          if (movies){
            this.favoritesMoviesQuanity = Object.keys(movies).length;
          }
        });
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
