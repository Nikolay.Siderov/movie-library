import { TestBed } from '@angular/core/testing';

import { LoggedUserGuard } from './logged-user.guard';
import { AuthenticationService } from '../../services/authentication.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { reducers } from 'src/app/store/reducers/favorite-movies.reducer';

describe('LoggedUserGuard', () => {
  let guard: LoggedUserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationService, LoggedUserGuard],
      imports: [RouterTestingModule, HttpClientModule, StoreModule.forRoot(reducers)]
    });
    guard = TestBed.inject(LoggedUserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
