import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Injectable()
export class LoggedUserGuard implements CanActivate {

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if(this.authService.token){
      this.router.navigateByUrl('/');
      return false;
    }else{
      return true;
    }
  }
}
