export const API_URL = {
    TMDB: 'https://api.themoviedb.org/3',
    favorites: 'https://practce-on-focus.firebaseio.com'
};
export const HTTP_PATHS = {
    favorites: '/favorites',
    json: '.json'
};

export const API_AUTH = {
    url: 'https://identitytoolkit.googleapis.com/v1/',
    routes: {
        signIn: 'accounts:signInWithPassword',
        signUp: 'accounts:signUp'
    },
    key: 'AIzaSyD15XONBwSn_kgvyJ7OE46Zt_CZ7_Yl6nM'
};

export const API_KEY = {
    TMDB: '519e9b151c1dc701bf50e6824fbe3409',
    favorites: 'AIzaSyD15XONBwSn_kgvyJ7OE46Zt_CZ7_Yl6nM'
};

export const QUERY = {
    key: '?key=',
    apiKey: '?api_key=',
    page: '&page=',
    query: '&query=',
};

export const LOCALID = 'localId';
export const TOKEN = 'TOKEN';
export const EMAIL_REGEX = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/i;
