import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMAIL_REGEX } from '../../constants';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit, OnDestroy {
  signUpForm: FormGroup;
  signUpResponse$: Subscription;
  signUpResponse: boolean = null;
  responseMessage: string;
  disableBtn = false;

  constructor(private auth: AuthenticationService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(EMAIL_REGEX)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      passwordConfirm: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.signUpResponse$ = this.auth.signUpResponse.subscribe(
      (response: { response: boolean; message?: string }) => {

        this.disableBtn = false;
        this.signUpResponse = response.response;

        if (this.signUpResponse) {
          this.signUpForm.reset();
        } else {
          this.responseMessage = response.message;
        }
      }
    );
  }

  submitSignUpForm(): void {
    this.disableBtn = true;
    this.auth.signUp({
      email: this.signUpForm.value.email,
      password: this.signUpForm.value.password,
    });
  }

  get validEmail(): boolean {
    return (
      this.signUpForm.controls.email.touched &&
      this.signUpForm.controls.email.invalid
    );
  }

  get validPassword(): boolean {
    return (
      this.signUpForm.controls.password.touched &&
      this.signUpForm.controls.password.invalid
    );
  }

  get passwordMatch(): boolean {
    if (
      this.signUpForm.controls.passwordConfirm.touched ||
      !this.signUpForm.controls.passwordConfirm.invalid
    ) {
      return (
        this.signUpForm.value.passwordConfirm !== this.signUpForm.value.password
      );
    }
  }

  get validateForm(): boolean {
    if (this.disableBtn){
      return false;
    }

    if (this.signUpForm.invalid) {
      return false;
    } else {
      if (!this.passwordMatch) {
        return true;
      }
    }
  }

  ngOnDestroy() {
    this.signUpResponse$.unsubscribe();
  }
}
