import { Injectable, Output, EventEmitter } from '@angular/core';
import { API_AUTH, TOKEN, QUERY, LOCALID } from '../constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ILoginForm, ILoginRequestResponse } from '../entities/login.interface';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ISignUpForm, ISignUpHttpResponse } from '../entities/sign-up.interace';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/entities/app-state';
import { userLogout } from 'src/app/store/actions/authentication.action';

@Injectable()
export class AuthenticationService {

  @Output()
  public isUserLogged: BehaviorSubject<any> = new BehaviorSubject(this.token);

  @Output()
  public loginFail: EventEmitter<any> = new EventEmitter();

  @Output()
  public signUpResponse: EventEmitter<any> = new EventEmitter();

  constructor(
    private http: HttpClient,
    private router: Router,
    private store: Store<IAppState>
  ) {}

  login(loginRequest: ILoginForm): void {
    this.http
      .post( `${API_AUTH.url}${API_AUTH.routes.signIn}${QUERY.key}${API_AUTH.key}`, loginRequest )
      .subscribe(
        (success: ILoginRequestResponse) => {
          sessionStorage.setItem(TOKEN, success.idToken);
          sessionStorage.setItem(LOCALID, success.localId);
          this.isUserLogged.next(this.token);
          this.router.navigateByUrl('/');
        },
        (error: HttpErrorResponse) => {
          this.loginFail.emit(true);
        }
      );
  }

  signUp(signUpForm: ISignUpForm) {
    this.http
      .post( `${API_AUTH.url}${API_AUTH.routes.signUp}${QUERY.key}${API_AUTH.key}`, signUpForm )
      .subscribe(
        (success: ISignUpHttpResponse) => {
          this.signUpResponse.emit({ response: true });
        },
        (error: HttpErrorResponse) => {
          this.signUpResponse.emit({
            response: false,
            message: error.error.error.message,
          });
        }
      );
  }

  logout(): void {
    sessionStorage.clear();
    this.store.dispatch(userLogout());
    this.isUserLogged.next(false);
  }

  get token(): string {
    return sessionStorage.getItem(TOKEN);
  }

  get localId(): string {
    return sessionStorage.getItem(LOCALID);
  }
}
